//
//  DetailViewController.swift
//  Movieleah
//
//  Created by Leah Vogel on 26/07/2018.
//  Copyright © 2018 Leah Vogel. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController {
    private var movie: Movie
    var backgroundImageView = UIImageView()
    var releaseView = DetailView()
    var overviewTextView = UITextView()
    let dateFormatter = DateFormatter()
    
    // MARK: - Lifecycle
    init(movie: Movie) {
        self.movie = movie
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        navigationItem.title = movie.title
        setupViews()

    }
    
    // MARK: - Other methods
    func setupViews() {
        let padding: CGFloat = 10
        backgroundImageView.contentMode = .scaleAspectFill
        backgroundImageView.alpha = 0.2
        if let posterPath = movie.posterPath, let url = URL(string: BASE_IMAGE_URL + posterPath) {
            backgroundImageView.kf.setImage(with: url, placeholder: PLACEHOLDER_IMAGE)
        } else {
            backgroundImageView.image = PLACEHOLDER_IMAGE
        }
        view.addSubview(backgroundImageView)
        backgroundImageView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            backgroundImageView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
            backgroundImageView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor),
            backgroundImageView.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor),
            backgroundImageView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor)
            ])
        
        releaseView.setText(title: "Release Date:", detail: dateString(dateString: movie.releaseDate))
        view.addSubview(releaseView)
        releaseView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            releaseView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: padding),
            releaseView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -padding),
            releaseView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 50),
            releaseView.heightAnchor.constraint(equalToConstant: 100)
            ])
        
        overviewTextView.isEditable = false
        overviewTextView.isSelectable = false
        overviewTextView.backgroundColor = .clear
        overviewTextView.text = movie.overview
        overviewTextView.font = UIFont.systemFont(ofSize: 20, weight: .regular)
        view.addSubview(overviewTextView)
        overviewTextView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            overviewTextView.topAnchor.constraint(equalTo: releaseView.bottomAnchor),
            overviewTextView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: padding),
            overviewTextView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -padding),
            overviewTextView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor)
            ])
    }
    
    
    func dateString(dateString: String?) -> String? {
        guard let dateString = dateString else {
            return nil
        }
        dateFormatter.dateFormat = "yyyy-mm-dd"
        if let date = dateFormatter.date(from: dateString) {
            dateFormatter.dateStyle = .long
            return dateFormatter.string(from: date)
        }
        return nil
    }
}
