//
//  ViewController.swift
//  Movieleah
//
//  Created by Leah Vogel on 26/07/2018.
//  Copyright © 2018 Leah Vogel. All rights reserved.
//

import UIKit
import Kingfisher

class ViewController: UIViewController {
    
    var tableView = UITableView()
    var searchBar = UISearchBar()
    var noResultsLabel = UILabel()
    var movies: [Movie] = [] {
        didSet {
            noResultsLabel.isHidden = movies.count > 0
            tableView.isHidden = movies.count == 0
            self.tableView.reloadData()
        }
    }
    let cellName = "MovieTableViewCell"
    let requestManager = RequestManager()

    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupSearchBar()
        setupTableView()
        setupNoResults()
        noResultsLabel.isHidden = true
        tableView.isHidden = true
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        searchBar.becomeFirstResponder()
    }
    
    // MARK: - Setup UI Elements
    func setupSearchBar() {
        searchBar.placeholder = "Type Movie Name"
        searchBar.delegate = self
        
        view.addSubview(searchBar)
        searchBar.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            searchBar.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
            searchBar.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            searchBar.trailingAnchor.constraint(equalTo: view.trailingAnchor)
            ])
        
    }
    
    func setupTableView() {
        tableView.register(UINib(nibName: cellName, bundle: nil), forCellReuseIdentifier: cellName)
        tableView.dataSource = self
        tableView.delegate = self
        
        view.addSubview(tableView)
        tableView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            tableView.topAnchor.constraint(equalTo: searchBar.bottomAnchor),
            tableView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            tableView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            tableView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor)
            ])
    }
    
    func setupNoResults() {
        noResultsLabel.text = "No Results"
        noResultsLabel.font = UIFont.boldSystemFont(ofSize: 20)
        noResultsLabel.textAlignment = .center
        noResultsLabel.layer.masksToBounds = true
        noResultsLabel.layer.cornerRadius = 12
        noResultsLabel.backgroundColor = .lightGray
        
        view.addSubview(noResultsLabel)
        noResultsLabel.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            noResultsLabel.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            noResultsLabel.centerYAnchor.constraint(equalTo: view.centerYAnchor),
            noResultsLabel.heightAnchor.constraint(equalToConstant: 100),
            noResultsLabel.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 0.8)
            ])
    }
}

// MARK: - Search Bar
extension ViewController: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        guard searchText.isEmpty == false else {
            self.movies = []
            self.noResultsLabel.isHidden = true
            return
        }
        requestManager.requestSearchMovies(searchString: searchText, completion: { (listings, error) in
            DispatchQueue.main.async {
                self.movies = listings?.sorted(by: {$0.title < $1.title}) ?? []
            }
        })
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
}

// MARK: - Table View
extension ViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return movies.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: cellName, for: indexPath) as? MovieTableViewCell else {
            return UITableViewCell()
        }
        let movie = movies[indexPath.row]
        cell.nameLabel.text = movie.title
        if let posterPath = movie.posterPath, let url = URL(string: BASE_IMAGE_URL+posterPath) {
            cell.posterImageView.kf.setImage(with: url, placeholder: PLACEHOLDER_IMAGE)
        } else {
            cell.posterImageView.image = PLACEHOLDER_IMAGE
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let movie = movies[indexPath.row]
        let detailViewController = DetailViewController(movie: movie)
        navigationController?.pushViewController(detailViewController, animated: true)
    }
}

