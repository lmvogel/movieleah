//
//  Constants.swift
//  Movieleah
//
//  Created by Leah Vogel on 26/07/2018.
//  Copyright © 2018 Leah Vogel. All rights reserved.
//

import Foundation
import UIKit

let API_KEY = "b8fd8777fe624f4c1439405ad57873cb"
let BASE_IMAGE_URL = "https://image.tmdb.org/t/p/w185/"
let PLACEHOLDER_IMAGE = UIImage(named: "placeholderImage")
