//
//  RequestManager.swift
//  Movieleah
//
//  Created by Leah Vogel on 26/07/2018.
//  Copyright © 2018 Leah Vogel. All rights reserved.
//

import Foundation

class RequestManager {
    let operationQueue = OperationQueue()

    init() {
        operationQueue.maxConcurrentOperationCount = 20
    }
    
    func requestMovieDetails(movieId: String, completion: @escaping (_ movie: Movie?, _ error: Error?) ->()) {
        guard let url = URL(string: "https://api.themoviedb.org/3/movie/\(movieId)?api_key=b8fd8777fe624f4c1439405ad57873cb&language=en-US") else {
            return
        }
        let request = URLRequest(url: url)
        let task = URLSession.shared.dataTask(with: request) { (data: Data?, urlResponse: URLResponse?, error: Error?) in
            guard let data = data, error == nil else {
                completion(nil, error!)
                return
            }
            
            do {
                let json = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as! [String:Any]
                if let movie = MovieParser.parseMovie(json: json) {
                    completion(movie, nil)
                } else {
                    completion(nil, nil)
                }
            } catch let error as NSError {
                print(error)
            }
        }
        task.resume()
    }
    
    func requestSearchMovies(searchString: String, completion: @escaping (_ listings: [Movie]?, _ error: Error?) ->()) {
        operationQueue.cancelAllOperations()
        operationQueue.addOperation(MovieSearchOperation(withSearchString: searchString, completion: completion))
    }
}
