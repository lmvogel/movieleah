//
//  MovieSearchOperation.swift
//  Movieleah
//
//  Created by Leah Vogel on 26/07/2018.
//  Copyright © 2018 Leah Vogel. All rights reserved.
//

import UIKit

class MovieSearchOperation: Operation {
    private var completion: ((_ movies: [Movie]?, _ error: Error?) -> ())?
    private let searchString: String

    private var _executing = false {
        willSet {
            willChangeValue(forKey: "isExecuting")
        }
        didSet {
            didChangeValue(forKey: "isExecuting")
        }
    }
    
    override var isExecuting: Bool {
        return _executing
    }
    
    private var _finished = false {
        willSet {
            willChangeValue(forKey: "isFinished")
        }
        
        didSet {
            didChangeValue(forKey: "isFinished")
        }
    }
    
    override var isFinished: Bool {
        return _finished
    }
    
    init(withSearchString searchString: String, completion: @escaping (_ listings: [Movie]?, _ error: Error?) ->()) {
        self.searchString = searchString
        self.completion = completion
    }
    
    override func main() {
        guard isCancelled == false else {
            finish(true)
            return
        }
        
        executing(true)
        requestSearchMovies(searchString: self.searchString) { (movies, error) in
            self.executing(false)
            self.finish(true)
            self.completion?(movies, error)
        }
    }
    
    func executing(_ executing: Bool) {
        _executing = executing
    }
    
    func finish(_ finished: Bool) {
        _finished = finished
    }
    
    
    func requestSearchMovies(searchString: String, completion: @escaping (_ listings: [Movie]?, _ error: Error?) ->()) {
        let adjustedSearchString = searchString.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed) ?? ""
        guard let url = URL(string: "https://api.themoviedb.org/3/search/movie?api_key=b8fd8777fe624f4c1439405ad57873cb&language=en-US&query=\(adjustedSearchString)&page=1&include_adult=false") else {
            return
        }
        let request = URLRequest(url: url)
        let task = URLSession.shared.dataTask(with: request) { (data: Data?, urlResponse: URLResponse?, error: Error?) in
            guard let data = data, error == nil else {
                completion(nil, error!)
                return
            }
            
            do {
                let json = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as! [String:Any]
                if let listings = MovieParser.parseList(json: json) {
                    completion(listings, nil)
                } else {
                    completion(nil, nil)
                }
            } catch let error as NSError {
                print(error)
            }
        }
        task.resume()
    }
}
