//
//  Models.swift
//  Movieleah
//
//  Created by Leah Vogel on 26/07/2018.
//  Copyright © 2018 Leah Vogel. All rights reserved.
//

import Foundation
import UIKit


struct Movie {
    var id: Int
    var title: String
    var posterPath: String?
    var releaseDate: String?
    var overview: String?
}

class MovieParser {
    static func parseMovie(json: [String: Any]) -> Movie? {
        var movie: Movie?
        if let title = json["title"] as? String, let id = json["id"] as? Int {
            movie = Movie(id: id, title: title, posterPath: json["poster_path"] as? String, releaseDate: json["release_date"] as? String, overview: json["overview"] as? String)
        }
        return movie
    }
    
    static func parseList(json: [String: Any]) -> [Movie]? {
        var listings: [Movie] = []
        if let results = json["results"] as? [[String: Any]] {
            results.forEach { (result) in
                if let movie = parseMovie(json: result) {
                    listings.append(movie)
                }
            }
        }
        return listings
    }
}
