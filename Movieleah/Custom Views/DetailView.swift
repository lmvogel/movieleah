//
//  DetailView.swift
//  Movieleah
//
//  Created by Leah Vogel on 26/07/2018.
//  Copyright © 2018 Leah Vogel. All rights reserved.
//

import UIKit

class DetailView: UIView {
    var titleLabel = UILabel()
    var detailLabel = UILabel()

    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    func commonInit() {
        titleLabel.font = UIFont.systemFont(ofSize: 20, weight: .bold)
        addSubview(titleLabel)
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            titleLabel.centerYAnchor.constraint(equalTo: centerYAnchor),
            titleLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 10),
            titleLabel.widthAnchor.constraint(equalTo: widthAnchor, multiplier: 0.4)
            ])
        
        detailLabel.font = UIFont.systemFont(ofSize: 18, weight: .regular)
        addSubview(detailLabel)
        detailLabel.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            detailLabel.centerYAnchor.constraint(equalTo: centerYAnchor),
            detailLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -10),
            detailLabel.leadingAnchor.constraint(equalTo: titleLabel.trailingAnchor, constant: 10)
            ])
    }
    
    func setText(title: String, detail: String?) {
        titleLabel.text = title
        detailLabel.text = detail == nil ? "Unknown" : detail!
    }
}
