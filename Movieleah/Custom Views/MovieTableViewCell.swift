//
//  MovieTableViewCell.swift
//  Movieleah
//
//  Created by Leah Vogel on 29/07/2018.
//  Copyright © 2018 Leah Vogel. All rights reserved.
//

import UIKit

class MovieTableViewCell: UITableViewCell {
    @IBOutlet weak var posterImageView: UIImageView!
    
    @IBOutlet weak var nameLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func prepareForReuse() {
        super.prepareForReuse()
        posterImageView.image = nil
        nameLabel.text = nil
    }
}
